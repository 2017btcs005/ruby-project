BEGIN {
    print ("beginning \n")
      }
print ("This is program body \n")

END{
    print ("this is end \n")
    system ("pause")
   }


print ("Enter your first_name:")
name=gets
puts (name)

print ("Enter your last_name:")
name=gets.chomp                       
puts (name)

# .chomp is used.in program for, without .chomp ruby lang. concate "\n" with string 
# here are example;
# find out the length of strings: are also clear concept of .chomp

print ("Enter your first_name:")
name=gets
puts (name.length)

print ("Enter your last_name:")
name=gets.chomp                       
puts (name.length)


