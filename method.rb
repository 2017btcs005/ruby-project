# methods in ruby
def func
    puts ("Hey bro...I am a function statement")
end
func

#methods with parameters in ruby

def func (my_data)
puts my_data
end

func("hi")
func("hello")
func("world")
func(23)
func(2.365)

# methods with parameters in ruby with concat string with int/float how to ?

def func (my_data)
        puts "my name is #{my_data}"     #concate string +integer in ruby.
 end
func("hi")
func("hello")
func("world")
func(23)
func(2.365)

system "pause"


