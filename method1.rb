#Method with default multiple parameter in ruby

def test (a="Ruby",b="perl",c="jvm")          #where test is function
    puts "The programming language is #{a}"
    puts "The programming language is #{b}"
    puts "The programming language is #{c}"
end
test
test "c" , "c++" ,"java" 

#returing values from method in ruby
def test          #where test is function
    i=10
    j=20
    k=30
    return i,j,k
end
x=test
puts x 

# Passing multiple parameters to a single method in ruby
def sample(*test)
    puts "the no. of parameters is #{test.length}"

    for i in 0...test.length               #  for loop using
        puts "The parameters are #{test[i]}"
    end
end
sample "world","12","M"
sample "zara","21","F","cse"
system "pause"


