=begin
Ruby Case statement:
SYNTAX
case expr0
when expr1,expr2
    stmt1
when expr3,expr4
    stmt2
else
    stmt3
end
=end

age =19
case age
when 0 ..2          #0,1,2
    puts "baby"
when 3 ..6          #3,4,5,6
    puts "little child"
when 7 ..12          #7,8,9,10,11
    puts "child"
when
    13 ..18          #13,14,15,16,17,18
    puts "youth"
else
    puts "adult"
end

system "pause"






