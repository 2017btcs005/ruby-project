# IF-ElSE statement:
x=1
if x<2
    puts "X is greater than 2"
else
    puts "X Is not Greater than 2"
end

# IF ...ElSE statement:

x=1
if x>2
    puts "X is greater than 2"
elsif x <= 2 and x!=1
    puts "X is 1"
else
    puts "I can't guess the number"
end

 #multiple line comment through this ways:--okkk.
=begin
Ruby if modifier:
SYNTAX:
code if condition
=end

x=1
print "x is 1\n" if x==1

 #multiple line comment through this ways:--okkk.
=begin
#Ruby unless statement:
#SYNTAX:
#unless conditional [then]
code
else
    code
end
=end
 
x=1
unless x>2
    puts "x is less than 2"
else
    puts "x is greater than 2"
end

 #multiple line comment through this ways:--okkk.
=begin       
Ruby unless modifier:
Syntax:
code unless condition
=end

x=false
puts "Value is false.Hence,statement executed" unless x 

system "pause"